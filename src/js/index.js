import $ from 'jquery'
import popper from 'popper.js'
import 'bootstrap'
import 'slick-carousel'
import '../scss/index.scss'

$(document).ready(function () {
    
    $('body').attr('style', '')

    function resizeSlider() {
        if($(window).width() > 1440) {
            let offset = $('.container-xl-custom').offset().left;
    
            $('.wrapper-carousel').css({
                'width' : $(window).width() - offset + 'px',
                'margin-left' : offset + 'px',
            })
        } else {
            $('.wrapper-carousel').attr('style', '');
        }
    }

    $('.cyclist').addClass('cyclist-offset');

    $('.checking').click(setOK)
    $('.form-check-label').click(setOK)

    function setOK() {
        if ($('.checking').attr('data-checked') === 'false') {
            $('.checking').attr('data-checked', 'true')
        } else $('.checking').attr('data-checked', 'false')
    }

    $('.button-poll-form').click(validateModal)

    function validateModal() {
        setClass();
        var pattern = /^.+@.+\..+$/igm;
        if ($('#name').val() === '') {
            $('.er-name').css('display', 'block')
            return false
        } else if ($('#email').val() === '') {
            $('.er-email').css('display', 'block')
            return false
        } else if (pattern.test(($('#email').val())) === false) {
            $('.er-email-rg').css('display', 'block')
            return false
        } else if ($('.checking').attr('data-checked') === 'false') {
            $('.er-ok').css('display', 'block')
            return false
        } else {
            // отправка заявки
            $('#exampleModal').modal('hide')
            setClass()
            let name = $('#name').val()
            let email = $('#email').val()
            $.ajax({
                type: 'POST',
                url: 'https://curierra.ru/mail.php',
                data: 'name=' + name + '&email=' + email,
                success: function() {
                    $('#modal-ok').modal('show')
                    setTimeout(function() {
                        $('#modal-ok').modal('hide')
                    }, 3000)
                    ym(64694935,'reachGoal','order')
                }
            })
        }
    }

    function setClass() {
        $('.er-name').css('display', 'none')
        $('.er-email').css('display', 'none')
        $('.er-email-rg').css('display', 'none')
        $('.er-ok').css('display', 'none')
    }

    $('.carousel').slick({
        infinite: false,
        slidesToShow: 2,
        dots: false,
        centerMode: false,
        variableWidth: true,
        prevArrow: $('.prev'),
        nextArrow: $('.next'),
        responsive: [
            {
                breakpoint: 575,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 0,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    $("#new-serv").click(function() {
        $('html, body').animate({
            scrollTop: $("#screen-new-serv").offset().top  
        }, 1000); 
    });

    $("#cour-says").click(function() {
        $('html, body').animate({
            scrollTop: $("#screen-couriers-says").offset().top  
        }, 1000); 
    });

    $("#your-opin").click(function() {
        $('html, body').animate({
            scrollTop: $("#screen-your-opinion").offset().top  
        }, 1000); 
    });

    $("#new-prod").click(function() {
        $('html, body').animate({
            scrollTop: $("#screen-new-product").offset().top  
        }, 1000); 
    });

    $('#message-go-send').click(validateMessage);

    function validateMessage() {
        setClassMes();
        var pattern = /^.+@.+\..+$/igm;
        if ($('#contact-name').val() === '') {
            $('.er-name-mes').css('display', 'block')
            return false
        } else if ($('#contact-email').val() === '') {
            $('.er-email-mes').css('display', 'block')
            return false
        } else if (pattern.test(($('#contact-email').val())) === false) {
            $('.er-email-rg-mes').css('display', 'block')
            return false
        } else if ($('#contact-message').val() === '') {
            $('.er-mes').css('display', 'block')
            return false
        } else {
            // отправка сообщения
            setClassMes();
            let name = $('#contact-name').val()
            let email = $('#contact-email').val()
            let comment = $('#contact-message').val()
            $.ajax({
                type: 'POST',
                url: 'https://curierra.ru/mail.php',
                data: 'name=' + name + '&email=' + email + '&comment=' + comment,
                success: function() {
                    $('#modal-ok').modal('show')
                    setTimeout(function() {
                        $('#modal-ok').modal('hide')
                    }, 3000)
                    ym(64694935,'reachGoal','feedback')
                }
            })
        }
    }

    function setClassMes() {
        $('.er-name-mes').css('display', 'none')
        $('.er-email-mes').css('display', 'none')
        $('.er-email-rg-mes').css('display', 'none')
        $('.er-mes').css('display', 'none')
    }

    resizeSlider();

    $(window).resize(resizeSlider);

    $('.close-modal-ok').click(function() {
        $('#modal-ok').modal('hide')
    })

    $('.navbar-toggler').click(bodyHidden)
    $('.close-button').click(bodyHidden)
    $('.nav-link').click(bodyHidden)

    function bodyHidden() {
        if($(window).width() < 992) {
            if($('.navbar-toggler').attr('aria-expanded') === 'false') {
                $('body').addClass('modal-open')
            } else {
                $('body').removeClass('modal-open')
            }
        } else {
            return false
        }
        
    }

    $('.modal').on('show.bs.modal', function() {
        $('body').attr('style', '')
    })
   
})